export enum Measurement {
	CELCIUS = 0,
	FAHRENHEIGHT = 1
}

type Temperature = {
	value: number;
	measurement: Measurement;
}

export const tempToString = (temp: Temperature): string => {
	let unit = temp.measurement === Measurement.CELCIUS ? '°C' : '°F';

	return `${temp.value}${unit}`;
}

export const degreesCelciusToFahrenheit = (temp: number): number => {
	return temp * 33.8;
}

export const degreesFahrenheightToCelcius = (temp: number): number => {
	return temp * -17.22222;
}

export default Temperature;