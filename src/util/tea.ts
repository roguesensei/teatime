import AsyncStorage from '@react-native-async-storage/async-storage';
import { ColorHex } from 'react-native-countdown-circle-timer';
import Temperature from './temperature';

type Tea = {
	id: string | number[],
	name: string,
	waterTemperature: Temperature,
	baseBrewTime: number,
	brewInfusionModifire: number,
	infusions: number,
	targetInfusions: number,
	colour: ColorHex
};

export const getTea = async (teaId: string): Promise<Tea | null> => {
	let json = await AsyncStorage.getItem(teaId);
	if (!json) {
		return null;
	}
	
	let tea: Tea = JSON.parse(json);
	return tea;
}

export const getAllTeas = async(): Promise<Tea[]> => {
	let teas: Tea[] = [];
	let keys = await AsyncStorage.getAllKeys();

	for (let i = 0; i < keys.length; i++) {
		let tea = await getTea(keys[i]);
		if (tea) {
			teas.push(tea);
		}
	}

	return teas;
}

export const calculateTeaBrewTime = (tea: Tea): number => {
	let time = tea.baseBrewTime;
	for (let i = 0; i < tea.infusions; i++) {
		time += tea.brewInfusionModifire;
	}
	return time;
}

export const saveTea = async (tea: Tea) => {
	let json = JSON.stringify(tea);

	await AsyncStorage.setItem(tea.id.toString(), json);
};

export const deleteTea = async (tea: Tea) => {
	await AsyncStorage.removeItem(tea.id.toString());
}

export default Tea;