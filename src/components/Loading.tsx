import React, { useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';

const Loading = (): JSX.Element => {
	return (
		<View style={{ alignContent: 'center', alignSelf: 'center', alignItems: 'center', flexDirection: 'row', flex: 1, flexWrap: 'wrap' }}>
			<Text style={{ fontSize: 32, textAlign: 'center' }}>
				Brewing...
			</Text>
		</View>
	)
}

const styles = StyleSheet.create({
	heading: {
		textAlign: 'center',
		fontWeight: 'bold',
		fontSize: 48
	},
	body: {
		fontSize: 36
	}
})

export default Loading;