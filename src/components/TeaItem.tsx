import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Tea from '../util/tea';
import { tempToString } from '../util/temperature';

type TeaProps = {
	tea: Tea;
	onPress?:() => void;
	onLongPress?: () => void;
}

const TeaItem = (props: TeaProps): JSX.Element => {
	let tea = props.tea;
	let temp = tempToString(tea.waterTemperature);

	return (
		<View style={{flex: 1}}>
			<TouchableOpacity
				// disabled={props.isTiming}
				style={styles.container}
				onPress={props.onPress}
				onLongPress={props.onLongPress}
			>
				<View style={[ styles.item, { borderWidth: 2, borderColor: tea.colour }]}>
					<Text style={styles.title}>
						{tea.name}
					</Text>
					<Text style={styles.text}>
						{temp}
					</Text>
					<Text style={styles.text}>
						Infusions: {tea.infusions}
					</Text>
				</View>
			</TouchableOpacity>
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	item: {
		padding: 20,
		marginVertical: 8,
		marginHorizontal: 16,
	},
	title: {
		fontSize: 32,
	},
	text: {
		fontSize: 16
	}
});


export default TeaItem;