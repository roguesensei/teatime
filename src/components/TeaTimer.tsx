import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native';
import { ColorHex, CountdownCircleTimer, TimeProps } from 'react-native-countdown-circle-timer';

type TeaProps = {
	i: number; // Iterations to reset timer (See https://github.com/vydimitrov/react-countdown-circle-timer/tree/master/packages/mobile#restart-timer-at-any-given-time)
	colour: ColorHex;
	countDown: number;
	isTiming: boolean;
	name: string;
	onStop: () => void
}

const TeaTimer = (props: TeaProps): JSX.Element => {
	const [maxCountdown, setMaxCountdown] = useState(props.countDown);
	const [countdown, setCountdown] = useState(props.countDown);
	const [isTiming, setIsTiming] = useState(props.isTiming);

	useEffect(() => {
		if (!props.isTiming || !isTiming) {
			setMaxCountdown(props.countDown);
			setCountdown(props.countDown);
		}
		setIsTiming(props.isTiming);
	}, [props]);

	const children = (timeProps: TimeProps) => {
		let remainingTime = timeProps.remainingTime;
		if (remainingTime === 0) {
			return <Text>Teatime!</Text>
		}

		// Pad number to be formatted as 00
		const pad = (num: number): string => {
			return num < 10 ? '0' + num.toString() : num.toString()
		}

		const minutes = Math.floor(remainingTime / 60);
		const seconds = remainingTime % 60;

		return <Text style={[styles.body, { color: timeProps.color }]}>{pad(minutes)}:{pad(seconds)}</Text>
	};

	return (
		<View style={{ flexDirection: 'column' }}>
			<Text style={[styles.heading, { color: props.colour }]}>
				{props.name}
			</Text>
			<CountdownCircleTimer
				key={props.i}
				isPlaying={isTiming}
				colors={props.colour}
				duration={maxCountdown}
				children={children}
				initialRemainingTime={countdown}
				size={256}
				onComplete={() => { props.onStop(); }}
			/>
		</View>
	)
}

const styles = StyleSheet.create({
	heading: {
		textAlign: 'center',
		fontWeight: 'bold',
		fontSize: 48
	},
	body: {
		fontSize: 36
	}
})

export default TeaTimer;