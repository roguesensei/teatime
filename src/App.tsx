import React, { useEffect, useState } from 'react';
import { Alert, Button, FlatList, ScrollView, View, useColorScheme } from 'react-native';
import Tea, { calculateTeaBrewTime, deleteTea, getAllTeas, saveTea } from './util/tea';
import { Measurement } from './util/temperature';
import Loading from './components/Loading';
import uuid from 'react-native-uuid';
import TeaTimer from './components/TeaTimer';
import TeaItem from './components/TeaItem';

let defaultTeas: Tea[] = [
	{
		id: uuid.v4(),
		name: 'Black Tea',
		waterTemperature: { value: 100, measurement: Measurement.CELCIUS },
		baseBrewTime: 120,
		brewInfusionModifire: 60,
		infusions: 0,
		targetInfusions: 4,
		colour: '#994d00'
	},
	{
		id: uuid.v4(),
		name: 'Green Tea',
		waterTemperature: { value: 70, measurement: Measurement.CELCIUS },
		baseBrewTime: 15,
		brewInfusionModifire: 3,
		infusions: 0,
		targetInfusions: 4,
		colour: '#00ff00'
	}
];

const getTeas = async (): Promise<Tea[]> => {
	let teas = await getAllTeas();
	if (teas.length === 0) {
		for (let i = 0; i < defaultTeas.length; i++) {
			await saveTea(defaultTeas[i]);
		}

		return defaultTeas;
	}
	return teas;
}

const App = (): JSX.Element => {
	const [loading, setLoading] = useState<boolean>(true);
	const [isTiming, setIsTiming] = useState<boolean>(false);
	const [teaIndex, setTeaIndex] = useState(0);
	const [tea, setTea] = useState<Tea>(defaultTeas[teaIndex]);
	const [teas, setTeas] = useState<Tea[]>(defaultTeas);
	const [timer, setTimer] = useState(calculateTeaBrewTime(tea));
	const [iterations, setIterations] = useState(0);

	const colourScheme = useColorScheme();
	
	const setTeaTimer = (t: Tea, i: number): void => {
		setTeaIndex(i);
		setTea(t);
		setTimer(calculateTeaBrewTime(t));
	}

	const reload = async () => {	
		setTeas(await getTeas());
		setTea(teas[0]);
		setLoading(false);
	}

	useEffect(() => {
		(async () => {
			await reload();
		})();
	}, []);

	if (loading) {
		return <Loading />
	}

	return (
		<View style={{ flexDirection: 'column', flex: 1, padding: 10 }}>
			<FlatList
				data={teas}
				keyExtractor={(x) => x.id?.toString()}
				renderItem={({item}) => 
					<TeaItem 
						tea={item}
						onPress={() => setTea(item)} 
						onLongPress={() => Alert.alert('Manage Tea', 'Edit or delete tea', [
							{
								text: 'Cancel',
								onPress: () => {}
							},
							{
								text: 'Delete',
								onPress: () => {
									deleteTea(item).then(() => reload());
								}
							},
							{
								text: 'Edit',
								onPress: () => {}
							}
						])}
					/>
				}
			/>
			<Button
				title={'Add Tea'}
				color={colourScheme === 'dark' ? 'grey' : 'blue'}
				onPress={() => {}}
			/>
			{/* <View style={{ height: '90%' }}>
				<TeaTimer
					i={iterations}
					colour={tea.colour}
					countDown={timer}
					name={tea.name}	
					isTiming={isTiming}
					onStop={() => {
						setIsTiming(false);
						
						let currentTea = tea;
						currentTea.infusions++;

						if (currentTea.infusions >= currentTea.targetInfusions) {
							tea.infusions = 0;
						}

						saveTea(currentTea).then(() => {
							let currentTeas = teas;
							currentTeas[teaIndex] = currentTea;
							
							setTeas(currentTeas);
							setTea(currentTea);
							setIterations(iterations + 1);
							setTimer(calculateTeaBrewTime(tea));
						});
					}}
				/>
			</View>
			<ScrollView
				style={{ flexDirection: 'row', height: '10%' }}
				horizontal={false}>
				{
					teas.map((t, i) => {
						return (
							<TeaView
								key={i}
								isTiming={isTiming}
								tea={teas[i]}
								onPress={() => {
									setTeaTimer(t, i);
								}}
								onLongPress={() => {
									setTeaTimer(t, i);
									setIsTiming(true);
								}}
							/>
						);
					})
				}
			</ScrollView> */}
		</View>
	)
}

export default App;
